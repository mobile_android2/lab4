import 'package:flutter/material.dart';
enum APP_THEME {LIGHT,DARK}
void main() => runApp(ContactProfilePage());

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.DARK;
  var currentThemAppBar = MyappTheme.appbarWidgetThemeLight();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: currentTheme==APP_THEME.DARK
            ? MyappTheme.appThemelight()
            : MyappTheme.appThemeDark(),
        home: Scaffold(
          appBar: currentTheme==APP_THEME.DARK
              ? MyappTheme.appbarWidgetThemeLight()
              : MyappTheme.appbarWidgetThemeDark(),
          body: bodyWiget(),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.threesixty),
            onPressed: (){
            setState((){
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
            },
            ),
          ),
        );
  }
}

///////////////////////////// Icon wiget in row
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.textsms,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money_rounded,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

/////////////////////////////// List wiget
Widget mobilePhoneListTite() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("330-803-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.black87,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTite() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("440-440-3390"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.black87,
      onPressed: () {},
    ),
  );
}

Widget emailListTite() {
  return ListTile(
    leading: Icon(Icons.email_outlined),
    title: Text("payut@go.buu.ac.th"),
    subtitle: Text("work"),
    trailing: IconButton(
      icon: Icon(Icons.alternate_email_rounded),
      color: Colors.black87,
      onPressed: () {},
    ),
  );
}

Widget locationListTite() {
  return ListTile(
    leading: Icon(Icons.add_location_outlined),
    title: Text("440-440-3390"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.black87,
      onPressed: () {},
    ),
  );
}

Widget bodyWiget() {
  return Scaffold(
    body: ListView(
      children: <Widget>[
        Column(
          children: [
            Container(
              width: double.infinity,
              height: 250,
              child: Image.network(
                "https://i.ytimg.com/vi/2O3Xf1gYwBs/mqdefault.jpg",
                fit: BoxFit.cover,
              ),
            ),
            Container(
              height: 60,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Prayut Chan-o-cha",
                      style: TextStyle(fontSize: 30),
                    ),
                  )
                ],
              ),
            ),
            Divider(
              color: Colors.grey,
            ),
            /*Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[Text("Hello row 2",
                      style: TextStyle(fontSize: 25 ),
                    )
                    ],
                  ),
                ),*/
            Container(
              margin: EdgeInsets.only(top: 8, bottom: 8),
              child: Theme(
                data: ThemeData(
                  iconTheme: IconThemeData(
                    color: Colors.red,
                  ),
                ),
                child: profileActionItem(),
              ),
            ),
            mobilePhoneListTite(),
            otherPhoneListTite(),
            emailListTite(),
            locationListTite(),
          ],
        )
      ],
    ),
  );
}

AppBar appbarWidget() {
  return AppBar(
    backgroundColor: Colors.white10,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.white38,
    ),
    actions: <Widget>[
      IconButton(
        onPressed: () {},
        icon: Icon(Icons.star_border),
        color: Colors.white38,
      )
    ],
  );
}
Widget profileActionItem(){
  return  Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}
class MyappTheme{
  static appThemelight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Colors.white,
            iconTheme: IconThemeData(color: Colors.black)));
}
  static  appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
            color: Colors.black,
            iconTheme: IconThemeData(color: Colors.black)));

  }
  static  appbarWidgetThemeDark() {
    return AppBar(
      backgroundColor: Colors.white10,
      leading: Icon(
        Icons.arrow_back,
        color: Colors.white38,
      ),
      actions: <Widget>[
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.star_border),
          color: Colors.white38,
        )
      ],
    );
  }

  static  appbarWidgetThemeLight() {
    return AppBar(
      backgroundColor: Colors.white,
      leading: Icon(
        Icons.arrow_back,
        color: Colors.black,
      ),
      actions: <Widget>[
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.star),
          color: Colors.black,
        )
      ],
    );
  }
}
